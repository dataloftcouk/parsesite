<?php
	///////////////////////////////////Инициализация///////////////////////////////////
	set_time_limit (0);
	ini_set("memory_limit", "2048M");
	
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	
	function myHandler($level, $message, $file, $line, $context) {
        // в зависимости от типа ошибки формируем заголовок сообщения
        switch ($level) {
            case E_WARNING:
                $type = 'Warning';
                break;
            case E_NOTICE:
                $type = 'Notice';
                break;
            default;
                // это не E_WARNING и не E_NOTICE
                // значит мы прекращаем обработку ошибки
                // далее обработка ложится на сам PHP
                return false;
        }
        // выводим текст ошибки
        echo "<h2>$type: $message</h2>";
        echo "<p><strong>File</strong>: $file:$line</p>";
        echo "<p><strong>Context</strong>: $". join(', $', array_keys($context))."</p>";
        // сообщаем, что мы обработали ошибку, и дальнейшая обработка не требуется
        return true;
    }
	
	include('../simple_html_dom.php');
	define('MAX_FILE_SIZE', 60000000);
	$table_name = 'plannings_all_deep';
	$linkDB = array();
	///////////////////////////////////End///////////////////////////////////
	$host = "127.0.0.1";
	$port = "5432";
	$dbname = "qw";
	$user = "pty";
	$password = "111";
	$dbconn = pg_connect("host={$host} port={$port} dbname={$dbname} user={$user} password={$password}") or die('Could not connect: ' . pg_last_error());
	$query = 'select link from public."' . $table_name . '" where link is not null and (download = 0 OR download is null)';
	$result = pg_query($query) or die('Ошибка запроса: ' . pg_last_error());
	$i = 0;
	while($line = pg_fetch_array($result, null, PGSQL_ASSOC)) 
	{
		$linkDB[$i] = $line['link'];
		$i++;
	}
	
	for($k = 0; $k < count($linkDB); $k++)
	{
		//echo $linkDB[$k] . "<br>";
		$url = curl_searchLinkDownload(trim($linkDB[$k]));
		//echo "<br>" . $url;
		if($url == "")
		{
			echo "<br><br>error " . trim($linkDB[$k]);
			$query = "UPDATE public." . $table_name . " SET download = 0 WHERE link = '{$linkDB[$k]}'";
			pg_query($query) or die('Ошибка запроса: ' . pg_last_error());
			continue;
		}
		$file_name = curl_download(trim($linkDB[$k]), $url);
		$query = "UPDATE public." . $table_name . " SET file_name = '{$file_name}', download = 1 WHERE link = '{$linkDB[$k]}'";
		pg_query($query) or die('Ошибка запроса: ' . pg_last_error());
	}
	
	echo "<br>" . "ehd parser process" . "<br>";
	
	//////////////////////////////////tests one url//////////////////////////////////////
	//$url = curl_searchLinkDownload('https://www.publicaccess.cherwell.gov.uk/online-applications/applicationDetails.do?activeTab=summary&keyVal=OWY591EML8B00');
	//echo $url;
	//$file_name = curl_download('https://www.publicaccess.cherwell.gov.uk/online-applications/applicationDetails.do?activeTab=summary&keyVal=OWY591EML8B00', $url);
	//$query = "UPDATE public.plannings_all_deep SET file_name = '{$file_name}', download = 1 WHERE link = 'http://www.southoxon.gov.uk/ccm/support/Main.jsp?MODULE=ApplicationDetails&REF=P17/S3916/FUL'";
	//$result = pg_query($query) or die('Ошибка запроса: ' . pg_last_error());
	//echo $query;
	//////////////////////////////////tests one url//////////////////////////////////////
	function normalizeURLdoc($urlTMP)//функция восстановления корректной последовательности аргументов. Для westoxon и oxford
	{
		$activeTab = substr($urlTMP, strpos($urlTMP, "&", "0") + 1);
		$keyVal = substr($urlTMP, strpos($urlTMP, "?", "0") + 1, strpos($urlTMP, "&", "0") - strpos($urlTMP, "?", "0") - 1);
		$clearUrl = substr($urlTMP, 0, strpos($urlTMP, "?", "0"));
		$normalizeURL = $clearUrl . "?" . $activeTab . "&" . $keyVal;
		return $normalizeURL;
		//вместе последовательности keyval=&activetab= будет activetab=&keyval=
	}
	function curl_searchLinkDownload($url)//функция для получения ссылки на скачивание (в зависимости от домена применяются разные подходы)
	{
		$result_url = "";
		if(substr_count($url, "publicaccess.westoxon.gov.uk") || substr_count($url, "public.oxford.gov.uk"))
		{
			$url = normalizeURLdoc($url);
			$url = str_replace("summary", "documents", $url);
			$url = $url . "&documentOrdering.orderBy=documentType&documentOrdering.orderDirection=ascending";
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.6 (KHTML, like Gecko) Chrome/16.0.897.0 Safari/535.6');
			curl_setopt($ch, CURLOPT_HEADER, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_REFERER, $url);
			$content = curl_exec($ch);
			curl_close($ch);

			$html = new simple_html_dom();
			$html->load($content);

			$table_tr = $html->find('tr');
			if(count($table_tr) == 0 || $table_tr == NULL)
			{
				echo "<br> problem {$url} <br>";
				return "";
			}
			foreach($table_tr as $tr)
			{
				$table_td = $tr->find('td');
				if(count($table_td))
				{
					if($table_td[2]->innertext == "Application Form")
					{
						$url_doc = $table_td[5]->find('a')[0];
						if($url_doc->href != NULL)
						{
							$tmpPos = strpos($url, '/', 10);
							$hostDoc = substr($url, 0, $tmpPos);
							$result_url = $hostDoc . $url_doc->href;
							break;
						}
					}						
				}
			}
		}
		else if(substr_count($url, "www.whitehorsedc.gov.uk") || substr_count($url, "www.southoxon.gov.uk"))
		{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.6 (KHTML, like Gecko) Chrome/16.0.897.0 Safari/535.6');
			curl_setopt($ch, CURLOPT_HEADER, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_REFERER, $url);
			$content = curl_exec($ch);
			curl_close($ch);
			
			$html = new simple_html_dom();
			$html->load($content);
			$table_divs = $html->find('#div.rowdiv');
			$flagExit = false;
			foreach($table_divs as $div)
			{
				$leftCell = $div->find('#div.leftcelldiv');
				if($leftCell != NULL || count($leftCell) != 0)
				{
					if(substr_count($leftCell[0]->innertext, "Downloads"))
					{
						$rightCell = $div->find('#div.rightcellmaxdiv');
						if($rightCell != NULL || count($rightCell) != 0)
						{
							$linksInCell = $rightCell[0]->find('a');
							if($linksInCell != NULL || count($linksInCell) != 0)
							{
								foreach($linksInCell as $link)
								{
									if(substr_count($link->innertext, "Application Form.pdf") || substr_count($link->innertext, "ApplicationForm.pdf") || substr_count($link->innertext, "Application_Form.pdf") || substr_count($link->innertext, "Application Form.PDF") || substr_count($link->innertext, "ApplicationForm.PDF") || substr_count($link->innertext, "Application_Form.PDF") || substr_count($link->innertext, "Applicatin Form.pdf") || substr_count($link->innertext, "ApplicatinForm.pdf") || substr_count($link->innertext, "Applicatin_Form.pdf") || substr_count($link->innertext, "Applicatin Form.PDF") || substr_count($link->innertext, "ApplicatinForm.PDF") || substr_count($link->innertext, "Applicatin_Form.PDF") || substr_count($link->innertext, "Application Form (") || substr_count($link->innertext, "Application Form_1.pdf") || substr_count($link->innertext, "Application Form .pdf") || substr_count($link->innertext, "ApplicationForm .pdf") || substr_count($link->innertext, "Application_Form .pdf") || substr_count($link->innertext, "Application Form_Redacted.pdf"))
									{
										$link->href;
										$tmpPos = strpos($url, '/', 10);
										$hostDoc = substr($url, 0, $tmpPos);
										$result_url = $hostDoc . $link->href;
										$flagExit = true;
										break;
									}
								}
							}
							else
							{
								echo "<br> problem with find tag a in leftcell {$url} <br>";
								return "";
							}
						}
						else
						{
							echo "<br> problem with find rightcellmaxdiv {$url} <br>";
							return "";
						}
					}
				}
				if($flagExit)
					break;
			}
			if($result_url == "")
			{
				echo "<br> problem {$url} <br>";
				return "";
			}
		}
		else if(substr_count($url, "www.publicaccess.cherwell.gov.uk"))
		{
			$tmpURL = "";
			$LinkTMP = "";
			$url = str_replace("summary", "externalDocuments", $url);
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.6 (KHTML, like Gecko) Chrome/16.0.897.0 Safari/535.6');
			curl_setopt($ch, CURLOPT_HEADER, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_REFERER, $url);
			$content = curl_exec($ch);
			curl_close($ch);
			
			$html = new simple_html_dom();
			$html->load($content);
			$tmpDocLink = $html->find("#p.externalDocumentsLink");
			if(count($tmpDocLink) <> 1 || $tmpDocLink == NULL)
			{
				echo "<br> problem with extract url for documents with website {$url} <br>";
				return "";
			}
			$linkOnDoc = $tmpDocLink[0]->find('a');
			if(count($linkOnDoc) != 0 || $linkOnDoc != NULL)
			{
				if(substr_count($linkOnDoc[0]->innertext, "View associated documents"))
				{
					$tmpURL = $linkOnDoc[0]->href;
				}
				else 
				{
					echo "<br> problem with extract url url{$url} <br>";
					return "";
				}
			}
			else 
			{
				echo "<br> problem with extract {$url} <br>";
				return "";
			}
			if($tmpURL != "")
			{
				$tmpPosition = 0;
				$SEARCH_TYP = substr($tmpURL, strpos($tmpURL, "=", "0") + 1, strpos($tmpURL, "&", "0") - strpos($tmpURL, "=", "0") - 1);
				$tmpPosition = strpos($tmpURL, "&", "0") + 1;
				$DOC_CLASS_COD = substr($tmpURL, strpos($tmpURL, "=", $tmpPosition
				) + 1, strpos($tmpURL, "&", $tmpPosition) - strpos($tmpURL, "=", $tmpPosition
				) - 1);
				$tmpPosition = strpos($tmpURL, "&", $tmpPosition) + 1;
				$APPLICATION_NUMBE = substr($tmpURL, strpos($tmpURL, "=", $tmpPosition
				) + 1);
				$data = array('SEARCH_TYPE' => $SEARCH_TYP, 'DOC_CLASS_CODE' => $DOC_CLASS_COD, 'APPLICATION_NUMBER' => $APPLICATION_NUMBE);
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_URL, "http://npa.cherwell.gov.uk/AniteIM.WebSearch/ExternalEntryPoint.aspx");
				curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0");
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				//curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
				curl_setopt($ch, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'].'/pdf/cherwell/cookieCherwell.txt');
				curl_setopt($ch, CURLOPT_HEADER, 1);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
				$content = curl_exec($ch);
				preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $content, $matches);
				$cookies = array();
				foreach($matches[1] as $item) {
					parse_str($item, $cookie);
					$cookies = array_merge($cookies, $cookie);
				}
				curl_close($ch);
				
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, "http://npa.cherwell.gov.uk/AniteIM.WebSearch/Results.aspx?grdResultsSort=Type%20Description&grdResultsSortDir=Asc&grdResultsPS=250");
				curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0");
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				//curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
				curl_setopt($ch, CURLOPT_REFERER, "http://npa.cherwell.gov.uk/AniteIM.WebSearch/Results.aspx");
				curl_setopt($ch, CURLOPT_HEADER, 1);
				curl_setopt($ch, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'].'/pdf/cherwell/cookieCherwell.txt');
				curl_setopt($ch, CURLOPT_COOKIE, "ASP.NET_SessionId=".$cookies['ASP_NET_SessionId']);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
				$content = curl_exec($ch);
				curl_close($ch);
				
				$html = new simple_html_dom();
				$html->load($content);
				$tableOne = $html->find('#div.AIMTableData')[0];
				if(count($tableOne) != 0 || $tableOne != NULL)
				{
					$table_tr = $tableOne->find('tr');
					if(count($table_tr) != 0 || $table_tr != NULL)
					{
						foreach($table_tr as $tr)
						{
							$table_td = $tr->find('td');
							if(count($table_td) != 0 || $table_td != NULL)
							{
								if(count($table_td) > 2)
								{
									if($table_td[2]->innertext == "App form " || $table_td[2]->innertext == "app form "  || $table_td[2]->innertext == "App form" || $table_td[2]->innertext == "app form")
									{
										$links_table = $table_td[0]->find('a');
										if(count($links_table) != 0 || $links_table != NULL)
										{
											if($LinkTMP == "")
											{
												$LinkTMP = "http://npa.cherwell.gov.uk/AniteIM.WebSearch/" . $links_table[0]->href;
											}
											else 
											{
												$LinkTMP = $LinkTMP . "ffffff" . "http://npa.cherwell.gov.uk/AniteIM.WebSearch/" . $links_table[0]->href;
											}
											
										}
										else 
										{
											echo "Problem with parse table row in url {$tmpURL}";
											return "";
										}
									}
								}
								else
								{
									//echo "debug {$tmpURL}";
								}
							}
							else
							{
								echo "Problem with parse table row in url {$tmpURL}";
								return "";
							}
						}
					}
					else
					{
						echo "Problem with parse table in url {$tmpURL}";
						return "";
					}
					if($LinkTMP != "")
					{
						/*var_dump($data);
						echo "primer";
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_URL, $LinkTMP);
						curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0");
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
						curl_setopt($ch, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'].'cookieCherwell.txt');
						curl_setopt($ch, CURLOPT_HEADER, 1);
						curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
						$content = curl_exec($ch);
						curl_close($ch);
						echo $content;*/
						$result_url = $tmpURL . "razdel" . $LinkTMP;
					}
					else
					{
						//echo "Problem void link {$tmpURL}";
						//echo "<br>";
						return "";
					}
				}
				else
				{
					//echo "<br>";
					//echo "not links for {$url}";
					//echo "<br>";
					return "";
				}
			}
		}
		
		$html->clear();
		unset($html);
		return $result_url;
	}
	function curl_download($url, $url_download)//$url ссылка со сраницы с которой документ $url_download ссылка на сам документ
	{
		if(substr_count($url, "cherwell.gov.uk"))
		{
			$original_url = $url;
			list($url, $url_DOC) = explode("razdel", $url_download);
			$ArrayUrls = explode("ffffff", $url_DOC);
			$resUrlsArray = Array();
			$returnFilesName = "";
			if(count($ArrayUrls) == 0)
			{
				$countElem = 1;
				$resUrlsArray[0] = $url_DOC;
			}
			else if(count($ArrayUrls) > 0)
			{
				$countElem = count($ArrayUrls);
				$resUrlsArray = $ArrayUrls;
			}
			$tmpPosition = 0;
			$SEARCH_TYP = substr($url, strpos($url, "=", "0") + 1, strpos($url, "&", "0") - strpos($url, "=", "0") - 1);
			$tmpPosition = strpos($url, "&", "0") + 1;
			$DOC_CLASS_COD = substr($url, strpos($url, "=", $tmpPosition) + 1, strpos($url, "&", $tmpPosition) - strpos($url, "=", $tmpPosition) - 1);
			$tmpPosition = strpos($url, "&", $tmpPosition) + 1;
			$APPLICATION_NUMBE = substr($url, strpos($url, "=", $tmpPosition) + 1);
			$data = array('SEARCH_TYPE' => $SEARCH_TYP, 'DOC_CLASS_CODE' => $DOC_CLASS_COD, 'APPLICATION_NUMBER' => $APPLICATION_NUMBE);
			
			for($cherwellCount = 0; $cherwellCount < $countElem; $cherwellCount++)
			{
				if($countElem == 1)
					$file_nameTMP = substr($original_url, strripos($original_url, "=") + 1) . ".pdf";
				else if($countElem > 1)
				{
					$tmpC = $cherwellCount + 1;
					$file_nameTMP = substr($original_url, strripos($original_url, "=") + 1) . " (" . $tmpC . ")" . ".pdf";
				}
				$returnFilesName = $returnFilesName . "$$" . $file_nameTMP;
				$dest_file = @fopen($_SERVER['DOCUMENT_ROOT']."/pdf/cherwell/{$file_nameTMP}", "w");
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_URL, $resUrlsArray[$cherwellCount]);
				curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0");
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
				curl_setopt($ch, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'].'/pdf/cherwell/cookieCherwell.txt');
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
				curl_setopt($ch, CURLOPT_FILE, $dest_file);
				$content = curl_exec($ch);
				fclose($dest_file);
				curl_close($ch);
			}
			return $returnFilesName;
		}
		else if(substr_count($url, "publicaccess.westoxon.gov.uk") || substr_count($url, "public.oxford.gov.uk"))
		{
			$url = normalizeURLdoc($url);
			$url = str_replace("summary", "documents", $url);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.6 (KHTML, like Gecko) Chrome/16.0.897.0 Safari/535.6');
			curl_setopt($ch, CURLOPT_HEADER, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_REFERER, $url);
			curl_setopt($ch, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'].'/pdf/public/cookiepublic.txt');
			$content = curl_exec($ch);
			curl_close($ch);
			//первая часть(которая сверху получает необходимые для работы cookie)
			if(substr_count($url, "publicaccess.westoxon.gov.uk"))
				$file_nameTMP = "publicaccess.westoxon.gov.uk " . substr($url, strripos($url, "=") + 1) . ".pdf";
			else if(substr_count($url, "public.oxford.gov.uk"))
				$file_nameTMP = "public.oxford.gov.uk " . substr($url, strripos($url, "=") + 1) . ".pdf";
			$dest_file = @fopen($_SERVER['DOCUMENT_ROOT']."/pdf/public/{$file_nameTMP}", "w");
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url_download);
			curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.6 (KHTML, like Gecko) Chrome/16.0.897.0 Safari/535.6');
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_REFERER, $url);
			curl_setopt($ch, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'].'/pdf/public/cookiepublic.txt');
			curl_setopt($ch, CURLOPT_FILE, $dest_file);
			$content = curl_exec($ch);
			curl_close($ch);
			fclose($dest_file);
			//вторая часть скачивает файл используя корректные cookie
			//все файлы сохраняются в server_root/pdf/
			return $file_nameTMP;
		}
		else if(substr_count($url, "www.whitehorsedc.gov.uk") || substr_count($url, "www.southoxon.gov.uk"))
		{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "http://www.whitehorsedc.gov.uk/java/support/dynamic_serve.jsp");
			curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.6 (KHTML, like Gecko) Chrome/16.0.897.0 Safari/535.6'); 
			curl_setopt($ch, CURLOPT_HEADER, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_REFERER, $url);
			//$fOut = fopen($_SERVER["DOCUMENT_ROOT"].'/'.'curl_out1.txt', "w" );
			//curl_setopt ($ch, CURLOPT_VERBOSE, 1);
			//curl_setopt ($ch, CURLOPT_STDERR, $fOut );
			curl_setopt($ch, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'].'/pdf/white_south/cookieWhiteSouth.txt');
			$content = curl_exec($ch);
			curl_close($ch);
			//первая часть(которая сверху получает необходимые для работы cookie)
			if(substr_count($url, "www.whitehorsedc.gov.uk"))
				$file_nameTMP = "www.whitehorsedc.gov.uk " . str_replace("/", " ", substr($url, strripos($url, "=") + 1)) . ".pdf";
			else if(substr_count($url, "www.southoxon.gov.uk"))
				$file_nameTMP = "www.southoxon.gov.uk " . str_replace("/", " ", substr($url, strripos($url, "=") + 1)) . ".pdf";
			$dest_file = @fopen($_SERVER['DOCUMENT_ROOT']."/pdf/white_south/{$file_nameTMP}", "w");
			$ch = curl_init();
			$url_download = str_replace("&amp;", "&", urldecode(trim($url_download)));
			curl_setopt($ch, CURLOPT_URL, $url_download);
			curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.6 (KHTML, like Gecko) Chrome/16.0.897.0 Safari/535.6');
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_REFERER, $url);
			//$fOut = fopen($_SERVER["DOCUMENT_ROOT"].'/'.'curl_out2.txt', "w" );
			//curl_setopt ($ch, CURLOPT_VERBOSE, 1);
			//curl_setopt ($ch, CURLOPT_STDERR, $fOut );
			curl_setopt($ch, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'].'/pdf/white_south/cookieWhiteSouth.txt');
			curl_setopt($ch, CURLOPT_FILE, $dest_file);
			$content = curl_exec($ch);
			curl_close($ch);
			fclose($dest_file);
			return $file_nameTMP;
		}
	}
?>