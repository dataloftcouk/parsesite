<?php
function get_links($link)
{
    // хранение результатов
    $ret = array();

    // создаем новый объект класса
    $dom = new domDocument;

    // получаем контент
    @$dom->loadHTML(file_get_contents($link));

    // убираем пробелы
    $dom->preserveWhiteSpace = false;

    // извлекаем все теги ссылок
    $links = $dom->getElementsByTagName('a');

    // получаем значение артибута href для всех ссылок
    foreach ($links as $tag) {
        $ret[$tag->getAttribute('href')] = $tag->childNodes->item(0)->nodeValue;
    }
    return $ret;
}

// в функцию пишим либо ссылку $link, либо файл (так как иногда по ссылке не получить код страницы, как на примере сслке выше)
//поэтому нужно загрузить стр. в браузере и вставить код в file.php
$urls = get_links('file.php');
// ссылка для сбора ссылок
//$urls = get_links('http://www.whitehorsedc.gov.uk/java/support/Main.jsp?MODULE=ApplicationCriteriaList&TYPE=Application&PARISH=ALL&AREA=&TXTSEARCH=&APP_TYPE=&APPTYPE=ALL&APP_STATUS=&SDAY=15&SMONTH=6&SYEAR=2018&EDAY=8&EMONTH=8&EYEAR=2018&Submit=Search');

// проверяем
if (sizeof($urls) > 0) {
    // выводим
    foreach ($urls as $key => $value) {

        echo 'https://publicaccess.westoxon.gov.uk/' . $key . '<br >';
//        echo 'https://www.publicaccess.cherwell.gov.uk' . $key . '<br >';
//        echo 'http://www.whitehorsedc.gov.uk/' . $key . '<br >';
        //echo 'http://public.oxford.gov.uk' . $key . '<br >';
        //echo $key . '<br >';
    }
} else {
    echo "Не удалось получить ссылки на странице $urls";
}