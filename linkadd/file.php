

























<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>

    <!-- #BeginEditable "doctitle" --><title>Search Results</title><!-- #EndEditable -->
    <!-- #BeginEditable "metatags" --><!-- #EndEditable -->

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <!-- Customer metatags -->
    <meta http-equiv="Content-Style-Type" content="text/css" /><meta http-equiv="Content-Script-Type" content="text/javascript" /><meta name="GENERATOR" content="Goss iCM - Intelligent Content Management" /><meta name="title" content="Residents" /><meta name="keywords" content="Health, well-being and care,Benefits,Allowances (welfare benefits), Claiming benefits, Social security payments, Welfare benefits, Welfare reform, Social benefits, Social welfare payments,Health, well-being and care,Benefits,Benefits,Benefits" /><meta name="DC.format.medium" content="html/text" /><meta name="eGMS.accessibility" content="Double-A" /><meta name="DC.title" content="Residents" /><meta name="DC.identifier" content="https://www.cherwell.gov.uk/index.cfm?articleid=1168" /><meta name="DC.language" scheme="ISO 639-2/T" content="En" /><meta name="DC.rights.copyright" content="Cherwell District Council" /><meta name="DC.type" scheme="e-GMSTES" content="Accounts" /><meta name="DC.coverage.spatial" content="Cherwell" /><meta name="eGMS.subject.category" scheme="IPSV" content="Benefits" /><meta name="eGMS.subject.category" scheme="IPSV" content="Benefits" /><meta name="eGMS.subject.keyword" content="Health, well-being and care,Benefits,Allowances (welfare benefits), Claiming benefits, Social security payments, Welfare benefits, Welfare reform, Social benefits, Social welfare payments,Health, well-being and care,Benefits,Benefits,Benefits" /><meta name="DC.contributor" content="" /><meta name="DC.creator" content="" /><meta name="DC.publisher" content="" />

    <!-- Favicon link -->
    <link rel="icon" href="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/cherwell/favicon.ico" type="image/x-icon" />

    <link rel="stylesheet" href="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/styles/screen.css" type="text/css" media="screen,projection" />
    <link rel="stylesheet" href="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/styles/theme1.css" type="text/css"/>
    <link rel="stylesheet" href="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/styles/print.css" type="text/css" media="print" />

    <script type="text/javascript" src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/scripts/jquery.min.js"></script>
    <script type="text/javascript" src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/scripts/jquery.toolbar.idox.min.js"></script>
    <script type="text/javascript" src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/scripts/common.js"></script>
    <script type="text/javascript" src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/scripts/checkbox.js"></script>
    <script type="text/javascript" src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/scripts/amplify.min.js"></script>
    <script type="text/javascript" src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/scripts/createCommentCookie.js"></script>

    <!-- Customer Analytics Script -->
    <script type="text/javascript">var _gaq = _gaq || []; _gaq.push(['_setAccount', 'UA-7444723-1']); _gaq.push(['_trackPageview']);(function() {var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);})();</script>

    <script type="text/javascript">
        $('html').addClass('js');
        $(document).ready(function(){
            $('.main#apply>span:first-child').attr('tabindex', '0')
        });
    </script>

    <!-- #BeginEditable "mobile" --><!-- #EndEditable -->
    <!-- #BeginEditable "head" --><!-- #EndEditable -->
    <!-- #BeginEditable "webapp" -->
    <script type="text/javascript">
        $(document).ready( function() {
            var form = document.getElementById("mapForm");
            form.submit();
        });
    </script>
    <!-- #EndEditable -->

</head>
<body>

<a href="#pageheading" class="sr-only">Skip to main content</a>

<!-- IDOX PA START -->
<div id="idox">
    <div id="pa">
        <div id="header">
            <div class="container">
                <div id="logo"><a href="https://www.cherwell.gov.uk/" title="Cherwell District Council home page"><img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/cherwell/logo.png" alt="Council logo" /><span class="sr-only">Cherwell District Council</span></a></div>
                <div id="headertitle"></div>
                <div class="clear"></div>
            </div>
        </div>
        <div id="breadcrumbs">
            <div class="container">
                <ul>
                    <li><a href="https://www.cherwell.gov.uk/">Home</a></li><li><span>Public Access</span></li>
                </ul>
                <div class="clear"></div>
            </div>
        </div>
        <div class="container">
            <div id="toolbar" class="nojs">
                <ul>
                    <li id="search" class="main"><span tabindex="0">Search&nbsp;</span><span class="caret"></span>
                        <ul>
                            <!-- #BeginLibraryItem "/Library/searchModulesNavItems.lbi" -->















                            <li id="planLinks">
                                <a href="/online-applications/search.do?action=simple&amp;searchType=Application" >
                                    <span>Planning</span>
                                </a>
                                <ul>
                                    <li id="planBasicSearch">
                                        <a href="/online-applications/search.do?action=simple&amp;searchType=Application">
                                            Simple Search
                                        </a>
                                    </li>

                                    <li id="planAdvancedSearch">
                                        <a href="/online-applications/search.do?action=advanced&amp;searchType=Application">
                                            Advanced
                                        </a>
                                    </li>
                                    <li id="planListSearch">
                                        <a href="/online-applications/search.do?action=weeklyList&amp;searchType=Application">
                                            Weekly/Monthly Lists
                                        </a>
                                    </li>
                                    <li id="planPropertySearch">
                                        <a href="/online-applications/search.do?action=property&amp;type=custom">
                                            Property Search
                                        </a>
                                    </li>


                                    <li id="planMapsearch">
                                        <a href="/online-applications/spatialDisplay.do?action=display&amp;searchType=Application">
                                            Map Search
                                        </a>
                                    </li>


                                </ul>
                            </li>



                            <li id="bcLinks">
                                <a href="/online-applications/search.do?action=simple&amp;searchType=BuildingControl">
                                    <span>Building Control</span>
                                </a>
                                <ul>
                                    <li id="bcBasicSearch">
                                        <a href="/online-applications/search.do?action=simple&amp;searchType=BuildingControl">
                                            Simple Search
                                        </a>
                                    </li>
                                    <li id="bcAdvancedSearch">
                                        <a href="/online-applications/search.do?action=advanced&amp;searchType=BuildingControl">
                                            Advanced
                                        </a>
                                    </li>
                                    <li id="bcListSearch">
                                        <a href="/online-applications/search.do?action=weeklyList&amp;searchType=BuildingControl">
                                            Weekly/Monthly Lists
                                        </a>
                                    </li>
                                    <li id="bcPropertySearch">
                                        <a href="/online-applications/search.do?action=property&amp;type=custom">
                                            Property Search
                                        </a>
                                    </li>


                                    <li id="bcMapsearch">
                                        <a href="/online-applications/spatialDisplay.do?action=display&amp;searchType=BuildingControl">
                                            Map Search
                                        </a>
                                    </li>


                                </ul>
                            </li>





                            <li id="liLinks">
                                <a href="/online-applications/search.do?action=simple&amp;searchType=LicencingApplication">
                                    <span>Licensing</span>
                                </a>

                                <ul>
                                    <li id="liBasicSearch">
                                        <a href="/online-applications/search.do?action=simple&amp;searchType=LicencingApplication">
                                            Simple Search
                                        </a>
                                    </li>
                                    <li id="liAdvancedSearch">
                                        <a href="/online-applications/search.do?action=advanced&amp;searchType=LicencingApplication">
                                            Advanced
                                        </a>
                                    </li>
                                    <li id="liPropertySearch">
                                        <a href="/online-applications/search.do?action=property&amp;type=custom">
                                            Property Search
                                        </a>
                                    </li>


                                    <li id="liMapsearch">
                                        <a href="/online-applications/spatialDisplay.do?action=display&amp;searchType=LicencingApplication">
                                            Map Search
                                        </a>
                                    </li>

                                </ul>
                            </li>
                            <!-- #EndLibraryItem -->
                        </ul>
                    </li>
                    <!-- #BeginEditable "applicationForms" -->






                    <!-- #EndEditable -->
                    <li id="myprofile" class="main"><span tabindex="0">My Profile&nbsp;</span><span class="caret"></span>
                        <ul>
                            <!-- #BeginLibraryItem "/Library/consulteeInTrayNavItem.lbi" -->






                            <!-- #EndLibraryItem -->
                            <li><a href="https://www.publicaccess.cherwell.gov.uk/online-applications/registered/displayUserDetails.do">Profile Details</a></li>
                            <li><a href="https://www.publicaccess.cherwell.gov.uk/online-applications/registered/savedSearch.do?action=display">Saved Searches</a></li>
                            <li><a href="https://www.publicaccess.cherwell.gov.uk/online-applications/registered/userAdmin.do?action=showNotifiedApplications">Notified Applications</a></li>
                            <li><a href="https://www.publicaccess.cherwell.gov.uk/online-applications/registered/trackedApplication.do?action=display">Tracked Applications</a></li>
                            <!-- #BeginEditable "formSubmissions" -->








                            <!-- #EndEditable -->
                        </ul>
                    </li>
                    <li id="loginLink" class="main"><!-- #BeginEditable "loginlogout" -->










                        <a href="/online-applications/registered/userAdmin.do">Login</a>
                        <!-- #EndEditable --></li>
                    <!-- #BeginEditable "register" -->











                    <li id="register">
                        <a href="/online-applications/registrationWizard.do?action=start">Register</a>
                    </li>

                    <!-- #EndEditable -->
                    <!-- #BeginLibraryItem "/Library/applyOnlineNavItems.lbi" -->






                    <li id="apply" class="main">
                        <span>Apply Online&nbsp;</span>
                        <span class="caret"/>
                    </li>
                    <!-- #EndLibraryItem -->
                </ul>
            </div>

            <!-- #BeginEditable "headlinenews" --><!-- #EndEditable -->

            <div id="pageheading">
                <h1><strong>Planning</strong>&nbsp;&ndash;&nbsp;<!-- #BeginEditable "pageheading" -->










                    Results for Application Search




                    <!-- #EndEditable --></h1>
                <!-- #BeginEditable "helplink" --><!-- #EndEditable -->
            </div>

            <!-- START WAM CONTENT -->
            <div class="content">
                <!-- #BeginEditable "errormsg" --><!-- #EndEditable -->
                <!-- #BeginEditable "pagehead" --><!-- #EndEditable -->
                <!-- #BeginEditable "tabs" --><!-- #EndEditable -->
                <!-- #BeginEditable "tabsubnav" --><!-- #EndEditable -->
                <!-- #BeginEditable "pagebody" -->





                <div id="searchtools">
                    <ul>



                        <li>
                            <a href="/online-applications/refineSearch.do?action=refine" id="refinesearch"><img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/button_refinesearch.gif" alt="Refine search icon" /></a>
                        </li>


                        <li>
                            <a href="/online-applications/registered/saveSearch.do?action=saveDialog" id="savesearch"><img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/button_savesearch.gif" alt="Save search icon" /></a>
                        </li>





                        <li>
                            <a href="/online-applications/pagedSearchResults.do?action=printPreview" rel="external" id="print" title="View a printable version of these search results (opens in a new window)">
                                <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/button_print.gif" alt="Print icon"/>
                            </a>
                        </li>

                    </ul>
                </div>




                <div id="searchfilters">

                    <form name="searchCriteriaForm" method="post" action="/online-applications/pagedSearchResults.do" id="searchResults">
                        <input type="hidden" name="searchCriteria.page" value="1" />
                        <input type="hidden" name="action" value="page" />


                        <span class="orderByContainer">
                        <label for="orderBy">
                            Sort by
                        </label>
                        <select name="orderBy" id="orderBy"><option value="Reference">Ref. No.</option>

                                <option value="DateReceived" selected="selected">Date Received</option>

                                <option value="Proposal">Description</option>

                                <option value="ExpiryDate">Expiry Date</option></select>
                    </span>


                        <span class="orderByDirectionContainer">
                    <label for="orderByDirection">
                        Direction
                    </label>
                    <select name="orderByDirection" id="orderByDirection"><option value="Ascending">Ascending</option>

                            <option value="Descending" selected="selected">Descending</option></select>
				</span>

                        <span class="resultsPerPage">
                    <label for="resultsPerPage">
                        Results per page
                    </label>
                    <select name="searchCriteria.resultsPerPage" id="resultsPerPage"><option value="5">5</option>
<option value="10">10</option>
<option value="20">20</option>
<option value="50">50</option>
<option value="100" selected="selected">100</option></select>
				</span>
                        <input type="submit" value="Go" class="button primary" />
                    </form>
                </div>




                <div id="searchResultsContainer" class="panel">







                    <p class="pager top"><span class="showing"><strong>Showing 1-100</strong> of 817</span><span class="divider">|</span><strong>1</strong><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=2" class="page">2</a><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=3" class="page">3</a><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=4" class="page">4</a><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=5" class="page">5</a><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=6" class="page">6</a><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=7" class="page">7</a><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=8" class="page">8</a><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=9" class="page">9</a><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=2" class="next">Next<img border="0" src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/pager_next.gif" alt="" title="Display the next page of search results"/></a></p>


                    <div class="col-a">

                        <ul id="searchresults">



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PDGL6XEMFQB00&amp;activeTab=summary">
                                    Discharge of conditions 3 (timber cladding) and 4 (doors and windows) of 17/00359/F
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Land North Of Wroxton Mill Street From Wroxton To Shutford Wroxton
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00326/DISC
                                    <span class="divider">|</span>










                                    Received:
                                    Tue 14 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Tue 14 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PDG539EM0L000&amp;activeTab=summary">
                                    Discharge conditions 3 (windows), 4 (windows), 5 (external door) and 10 (roof lights) of 15/02300/LB
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Home Farm Blacklocks Hill Nethercote Banbury OX17 2BW
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00325/DISC
                                    <span class="divider">|</span>










                                    Received:
                                    Tue 14 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Tue 14 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PDG4XHEM0L000&amp;activeTab=summary">
                                    Discharge conditions 3 (windows), 4 (windows) and 5 (external door) of 15/02299/F
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Home Farm Blacklocks Hill Nethercote Banbury OX17 2BW
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00324/DISC
                                    <span class="divider">|</span>










                                    Received:
                                    Tue 14 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Tue 14 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PDG9M2EM0L000&amp;activeTab=summary">
                                    Single storey rear extension - height to eaves 2.54m, overall height 2.77m, length 5m
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    27 Broad Close Kidlington OX5 1BE
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01449/HPA
                                    <span class="divider">|</span>










                                    Received:
                                    Tue 14 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Wed 15 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PDEH9XEM0L000&amp;activeTab=summary">
                                    Change the window on the side elevation of plot 1 (show home) from bay window to normal window (proposed as non material amendment to application 05/01337/OUT)
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    OS Parcel 6900 East Of Oxford Road And Adjoining South Of Canal Lane Bodicote
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00103/NMA
                                    <span class="divider">|</span>










                                    Received:
                                    Fri 10 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Tue 14 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PDEI6UEM0L000&amp;activeTab=summary">
                                    Non-Material Amendment to 10/01642/OUT - Substitute certain drawings approved under condition 1 And amendments to application 15/01209/REM - House Type substitution to 5 plots.
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Parcel B3 Adj To Camp Road And North Of Dacey Drive Upper Heyford
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00104/NMA
                                    <span class="divider">|</span>










                                    Received:
                                    Fri 10 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Fri 10 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PD8OI1EM0N400&amp;activeTab=summary">
                                    Non-Material Amendment to 18/00091/F - Amendment is sought to the landscape bund
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Symmetry Park Morrell Way Ambrosden
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00102/NMA
                                    <span class="divider">|</span>










                                    Received:
                                    Thu 09 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Fri 10 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PD7BUHEMFPB00&amp;activeTab=summary">
                                    Discharge of condition 3 (external lighting) of 18/00091/F
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Symmetry Park Morrell Way Ambrosden
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00319/DISC
                                    <span class="divider">|</span>










                                    Received:
                                    Thu 09 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Thu 09 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PD8LW3EM0N400&amp;activeTab=summary">
                                    Proposed Network Rail (East West Rail Bicester to Bedford Improvements) Order
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    East West Rail Network Phase 2 The Scheme Railway From London Road Bicester To Station Road Launton
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01430/TWA
                                    <span class="divider">|</span>










                                    Received:
                                    Thu 09 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Thu 09 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PD53XYEM0N400&amp;activeTab=summary">
                                    Discharge of condition 23 (BREEAM requirement) of 17/00958/F
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Former Tesco Pingle Drive Bicester OX26 6WA
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00315/DISC
                                    <span class="divider">|</span>










                                    Received:
                                    Wed 08 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Wed 08 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PD74EOEM0N400&amp;activeTab=summary">
                                    Scoping opinion for proposed development of up to 400 dwellings
                                </a>









                                <p class="address">


                                    Land Opposite Hanwell Fields Recreation Adj To Dukes Meadow Drive Banbury
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00003/SCOP
                                    <span class="divider">|</span>










                                    Received:
                                    Wed 08 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Wed 08 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PD5B02EM0N400&amp;activeTab=summary">
                                    Discharge of condition 14 (internal noise levels) of 14/02156/OUT
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Land South Of Cotefield Business Park Phase 2 Adj To Blossom Field Road Bodicote
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00316/DISC
                                    <span class="divider">|</span>










                                    Received:
                                    Wed 08 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Wed 08 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PD6T9XEM0N400&amp;activeTab=summary">
                                    Non-Material Amendment to 10/01642/OUT - Substitute certain drawings approved under condition 1 - Minor adjustment of rear access / boundary fence to plots 362 to 372 and 379-382. Minor adjustment of rear access to plot 385. Additional visitor parking bays to the side of plot 377 and opposite plots 405/406. Amended floor plan and associated fenestration changes to House Type SP8 on plots 365, 369, 378, 381, 405 and 408 and amended floor plan and associated fenestration changes to House Type SP9 on plots 366-368, 379-380 and 406-407
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Heyford Park Camp Road Upper Heyford Bicester OX25 5HD
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00101/NMA
                                    <span class="divider">|</span>










                                    Received:
                                    Wed 08 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Thu 09 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PD744WEM0N400&amp;activeTab=summary">
                                    Screening Opinion - Development of up to 400 dwellings
                                </a>









                                <p class="address">


                                    Land Opposite Hanwell Fields Recreation Adj To Dukes Meadow Drive Banbury
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00062/SO
                                    <span class="divider">|</span>










                                    Received:
                                    Wed 08 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Wed 08 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PD5AMXEM0N400&amp;activeTab=summary">
                                    Non-Material Amendment to 13/00847/OUT - Amend wording of condition 33 (fire hydrants)
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Phase 2 SW Bicester Parcel 7849 North Of Whitelands Farm Adjoining Middleton Stoney Road Bicester
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00100/NMA
                                    <span class="divider">|</span>










                                    Received:
                                    Tue 07 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Wed 08 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PD5ETZEM0CD00&amp;activeTab=summary">
                                    FIVE DAY NOTICE T1 x Beech tree Tag no T5883 - Remove
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Bletchingdon Park House Springwell Hill Bletchingdon Kidlington OX5 3DW
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00208/TCA
                                    <span class="divider">|</span>










                                    Received:
                                    Mon 06 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Mon 06 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Decided







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PDGB09EM0LX00&amp;activeTab=summary">
                                    T1 x Willow - Fell
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    The Boma Wroxton Lane Horley Banbury OX15 6BD
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00216/TCA
                                    <span class="divider">|</span>










                                    Received:
                                    Mon 06 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Mon 06 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PDGCNNEM0LX00&amp;activeTab=summary">
                                    T1 x Tulip, T2 x Gleditsia, T4 x Apple, T6 x Portuguese Laurel - Fell T3 x Maple - Crown raise to 3.0m, remove revirted growth T5 x Horse Chestnut - Crown raise 2.0m
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    East End Farm East End Hook Norton
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00217/TCA
                                    <span class="divider">|</span>










                                    Received:
                                    Mon 06 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Mon 06 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCW7VDEMFO200&amp;activeTab=summary">
                                    Replace window with door and replace window to front elevation
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Cotswold Cottage Main Street Upper Tadmarton Banbury OX15 5SH
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01396/LB
                                    <span class="divider">|</span>










                                    Received:
                                    Fri 03 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Fri 03 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PDG1REEM0LX00&amp;activeTab=summary">
                                    T1 x Walnut - Crown raise up to 2.0m to allow light to base and allow clearance of public right of way, remove stubbed branches to remove excessive regrowth, reduce extended lower crown limbs to improve aesthetics after crown raise.
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Bloxham School Banbury Road Bloxham Banbury OX15 4PE
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00215/TCA
                                    <span class="divider">|</span>










                                    Received:
                                    Fri 03 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Fri 03 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PDG0MQEM0LX00&amp;activeTab=summary">
                                    T1 x Walnut - Fell due to excessive size, shade and damage to building. Replant with more suitable species once garden area is landscaped.
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Wilberforce House Bloxham School Banbury Road Bloxham Banbury OX15 4PE
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00214/TCA
                                    <span class="divider">|</span>










                                    Received:
                                    Fri 03 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Fri 03 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PDEM39EM0LX00&amp;activeTab=summary">
                                    T1 x Fir - Fell as cracked stone wall, pushing it over, along with gate post and also encroaching on electricity cables above.
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Kingsclere Cottage Station Road Lower Heyford Bicester OX25 5PE
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00211/TCA
                                    <span class="divider">|</span>










                                    Received:
                                    Fri 03 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Fri 03 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PD1BI8EM0N400&amp;activeTab=summary">
                                    Non Material Amendment to 17/00669/REM - Alteration of the T3 house type internal floor arrangement from a 2 bed house type, to a 3 bed house type. An additional windows has also been introduced above the ginnel on the front and rear elevations.
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    OS Parcels 1200 3100 2000 1981 South Of Salt Way Bloxham Road Banbury
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00097/NMA
                                    <span class="divider">|</span>










                                    Received:
                                    Fri 03 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Mon 06 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCUD6SEMFNI00&amp;activeTab=summary">
                                    Discharge of condition 3 (means of access) of 17/02468/REM
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Phase 2 SW Bicester Parcel 7849 North Of Whitelands Farm Adjoining Middleton Stoney Road Bicester
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00313/DISC
                                    <span class="divider">|</span>










                                    Received:
                                    Thu 02 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Thu 02 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PD4WT3EM0N400&amp;activeTab=summary">
                                    T1 Ash tree - reduce tree by 4m and sides by 3m raise canopy over buildings and clear by 3m, raise crown by 5m above ground level - subject to TPO 20/1988
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    10 Landscape Close Weston On The Green Bicester OX25 3SX
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01417/TPO
                                    <span class="divider">|</span>










                                    Received:
                                    Thu 02 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Thu 02 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCUD7QEMFNN00&amp;activeTab=summary">
                                    Demolition of existing vacant workshop and show room buildings. Erection of two and three storey building to provide 10no. dwellings (8 x 2-bed and 2 x 1-bed). Provision of off-street car parking, secure cycle storage and covered refuse/recycling store - resubmission of 18/00130/F
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Kings Two Wheel Centre 139 Oxford Road Kidlington OX5 2NP
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01388/F
                                    <span class="divider">|</span>










                                    Received:
                                    Thu 02 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Thu 02 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCU4QXEM0N100&amp;activeTab=summary">
                                    2 No non-illuminated logo panel signs to match existing
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Polo Ralph Lauren 143 Pingle Drive Bicester OX26 6EU
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01385/ADV
                                    <span class="divider">|</span>










                                    Received:
                                    Thu 02 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Thu 02 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCUD8DEMFNP00&amp;activeTab=summary">
                                    T1 x Lime - Reduce by 20% keeping a natural shape, thin inner canopy removing epircormic growth, remove dead wood (exempt) as tree is situated on the edge of the property and overhangs a public bridleway.
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Eastgate House Eastgate Hornton Banbury OX15 6BT
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00206/TCA
                                    <span class="divider">|</span>










                                    Received:
                                    Thu 02 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Fri 03 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCUD8REMFNQ00&amp;activeTab=summary">
                                    Discharge of conditions 3 (materials), 4 (access proposals), 5 (construction traffic management statement) and 6 (landscape design) of 16/02550/F
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    65 Oxford Road Kidlington OX5 2BS
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00314/DISC
                                    <span class="divider">|</span>










                                    Received:
                                    Thu 02 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Fri 10 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCW2PPEM0N400&amp;activeTab=summary">
                                    Discharge of Condition 7 (lighting details) of 13/01798/CM
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Grundon Waste Management Waste Recycling And Transfer Complex Thorpe Mead Banbury OX16 4RZ
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00004/CDISC
                                    <span class="divider">|</span>










                                    Received:
                                    Thu 02 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Thu 02 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PDEI8SEM0LX00&amp;activeTab=summary">
                                    T1 x Ash - Removal of secondary stem from boundary tree as will raise the canopy over garden and field.
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Preedys Farm Street From Main Street To Swalcliffe Lea Upper Tadmarton Banbury OX15 5TB
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00210/TCA
                                    <span class="divider">|</span>










                                    Received:
                                    Wed 01 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Wed 01 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PDEEXWEM0CD00&amp;activeTab=summary">
                                    T1 and T2 - Cedar - Fell outgrown location very close together very close to oil tank. T3 - Birch - removal of first limb over extended and unbalancing crown
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Green Acres Main Street Sibford Gower Banbury OX15 5RW
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00209/TCA
                                    <span class="divider">|</span>










                                    Received:
                                    Wed 01 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Wed 01 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCSIICEMFN900&amp;activeTab=summary">
                                    Variation of condition 2 (plans) and 3 (car parking plan) of 17/02227/F - alterations to the approved extension, including increasing its depth by 1m at both ground and first floor and reduction in car parking spaces
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    13 Bletchingdon Road Kirtlington Kidlington OX5 3HG
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01382/F
                                    <span class="divider">|</span>










                                    Received:
                                    Wed 01 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Wed 01 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCSII0EMFN800&amp;activeTab=summary">
                                    Discharge of conditions 8 (levels) 17 (tree pits) 18 (tree pits) of 16/02428/REM
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Land Adjoining And West Of Warwick Road Banbury
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00309/DISC
                                    <span class="divider">|</span>










                                    Received:
                                    Wed 01 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Wed 01 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCSIK0EMFNE00&amp;activeTab=summary">
                                    Discharge of conditions 6 (landscaping) and 7 (landscaping) of 17/00855/F
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    The Oxfordshire Inn Meadow Walk Heathfield Kidlington OX5 3FG
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00312/DISC
                                    <span class="divider">|</span>










                                    Received:
                                    Wed 01 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Wed 01 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCSIJSEMFND00&amp;activeTab=summary">
                                    Alterations and extension to existing dwelling
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Ascona Orchard Lane Upper Heyford Bicester OX25 5LD
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01384/F
                                    <span class="divider">|</span>










                                    Received:
                                    Wed 01 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Wed 01 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCSIIZEMFNB00&amp;activeTab=summary">
                                    Discharge of condition 17 (landscape protection) 23 (surface water drainage) 51 (plant storage) 52 (wheel washing facilities) 53 (turning areas) of 10/01642/OUT
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Village Centre NorthHeyford Park Camp Road Upper Heyford Bicester OX25 5HD
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00311/DISC
                                    <span class="divider">|</span>










                                    Received:
                                    Wed 01 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Wed 01 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCSIILEMFNA00&amp;activeTab=summary">
                                    Discharge of condition 4 (CEMP) 16 (CTMP) of 18/00513/REM
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Village Centre North Heyford Park Camp Road Upper Heyford Bicester OX25 5HD
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00310/DISC
                                    <span class="divider">|</span>










                                    Received:
                                    Wed 01 Aug 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Wed 01 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCQNUYEMFN100&amp;activeTab=summary">
                                    A tree and land survey has been carried out for the entire Estate, with every tree being tagged, their locations plotted on maps, and information recorded (tree type, dia, height and spread). Additional more detailed surveys have been carried out for trees in areas where significant development works are planned and a BS5837 Arboricultural Report has been prepared by Pryor and Rickett. There are no tree works planned to any trees with an existing TPO. A detailed programme of tree works has been prepared as part of the imminent submission of planning applications for the Landscape Masterplan for the Bletchingdon Park Estate. The identified works include tree thinning operations which are to be carried out under a tree felling license obtained for Bletchingdon Park from the Forestry Commission (South East and London) (Application Ref 019/192/16-17, valid from 11 Aug 2016 until 10 Aug 2021). An overview map of the proposed tree works programme in the Outer Park area is shown in drawing BLLP 405. Further detailed tree work maps (1-6) are shown where the main areas for works are planned to be carried out, including the crescent pool area, the cascade pool area and the pump house pool area. These maps provide details of tree tag no, tree type and relevant tree information as well as showing whether trees are to remain, to be felled, to be transplanted or to have formative pruning carried out.
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Bletchingdon Park House Springwell Hill Bletchingdon Kidlington OX5 3DW
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00203/TCA
                                    <span class="divider">|</span>










                                    Received:
                                    Tue 31 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Tue 31 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCQF58EM0N100&amp;activeTab=summary">
                                    Single storey rear extension - depth 6m, height to eaves 2.4m and overall height 2.8m
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    47 Neithrop Avenue Banbury OX16 2NT
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01375/HPA
                                    <span class="divider">|</span>










                                    Received:
                                    Tue 31 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Tue 07 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCQNVSEMFN200&amp;activeTab=summary">
                                    Oak (T27) - Reduction of the lowest overhanging branches to plot 5 back to dividing boundary and the highest branches reduced back by 2m. A crown clean to reduce dead, diseased and duplicated wood - subject to TPO 10/2014
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Adj To 5 Noral Close Banbury OX16 2DW
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01378/TPO
                                    <span class="divider">|</span>










                                    Received:
                                    Tue 31 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Tue 31 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCQJJREM0CD00&amp;activeTab=summary">
                                    FIVE DAY NOTICE T1 x Beech - Fell
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    The Old Barn House Cumberford Bloxham Banbury OX15 4QG
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01377/TCA_5
                                    <span class="divider">|</span>










                                    Received:
                                    Tue 31 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Tue 31 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Decided







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCQNWLEMFN400&amp;activeTab=summary">
                                    Discharge of Conditions 8 (Landscaping), 10 (Landscape Management Plan), 13 (Bat survey), 14 (enhancing biodiversity), 15 (Bat and bird boxes) and 17 (WSI) of 18/00366/F
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Akeman Spinney Heyford Road Kirtlington Kidlington OX5 3HS
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00308/DISC
                                    <span class="divider">|</span>










                                    Received:
                                    Tue 31 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Tue 31 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCRX6XEM0K200&amp;activeTab=summary">
                                    Non-material amendment to 16/02482/REM - site layout amended to accomodate plot substitution of 47 No plots with 3 No house types
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Land South West Of Bicester Adjoining Oxford Road And Middleton Stoney Road Bicester
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00095/NMA
                                    <span class="divider">|</span>










                                    Received:
                                    Tue 31 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Tue 31 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Decided







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCSAINEM0N100&amp;activeTab=summary">
                                    T1 (Maple) - Bacterial cankers (60-70% crown dead and dying, fell for public safety)
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    The Coach House 6 St Johns Road Banbury OX16 5HX
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01380/TPO_5
                                    <span class="divider">|</span>










                                    Received:
                                    Tue 31 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Wed 01 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Decided







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCQNW5EMFN300&amp;activeTab=summary">
                                    Erection of Single Storey Extension within existing Lightwell to form flexible use space/enlarged school hall, associated amendments to existing external glazing to suit internal modifications.
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    William Morris County Primary School Bretch Hill Banbury OX16 0UZ
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01379/F
                                    <span class="divider">|</span>










                                    Received:
                                    Tue 31 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Wed 15 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCU8AXEM0CD00&amp;activeTab=summary">
                                    T1 x Giant Redwood - Fell and replace with suitable shrub
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Driftaway Manor Orchard Horley Banbury OX15 6DZ
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00204/TCA
                                    <span class="divider">|</span>










                                    Received:
                                    Mon 30 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Mon 30 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCOT15EM0N400&amp;activeTab=summary">
                                    Screening opinion to 18/01333/F - Extension to existing Technical Site to provide new employment units comprising flexible B1(c) light industrial, B2 (general industrial), B8 (storage or distribution) uses with ancillary offices, storage, display and sales, together with associated access, parking and landscaping.
                                </a>









                                <p class="address">


                                    Bicester Heritage Buckingham Road Bicester
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00061/SO
                                    <span class="divider">|</span>










                                    Received:
                                    Mon 30 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Mon 30 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCQ0ROEM0N400&amp;activeTab=summary">
                                    Discharge of condition 6 (zero carbon off site) of 17/00573/CDC
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Eco Business Centre Charlotte Avenue Bicester OX27 8BL
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00307/DISC
                                    <span class="divider">|</span>










                                    Received:
                                    Mon 30 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Mon 30 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCOT7OEMFMT00&amp;activeTab=summary">
                                    Discharge of condition 3 (window details) of 18/00906/LB
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Flat 4 Enhance Court 12 - 13 Horse Fair Banbury OX16 0AH
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00305/DISC
                                    <span class="divider">|</span>










                                    Received:
                                    Mon 30 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Mon 30 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCOT7QEMFMU00&amp;activeTab=summary">
                                    Discharge of condition 3 (window details) of 18/00907/F
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Flat 4 Enhance Court 12 - 13 Horse Fair Banbury OX16 0AH
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00306/DISC
                                    <span class="divider">|</span>










                                    Received:
                                    Mon 30 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Mon 30 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCOT7TEMFMV00&amp;activeTab=summary">
                                    Outbuilding to be used as a store, office and gym
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    5 Foundation Square Ambrosden Bicester OX25 2AQ
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01372/F
                                    <span class="divider">|</span>










                                    Received:
                                    Mon 30 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Tue 14 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCOT7FEMFMQ00&amp;activeTab=summary">
                                    Erect white Pvcu conservatory to side of property
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    35 Kempton Close Bicester OX26 1AD
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01369/F
                                    <span class="divider">|</span>










                                    Received:
                                    Mon 30 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Mon 30 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCOJ2JEM0N400&amp;activeTab=summary">
                                    Single storey front extension
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    27 Rochester Way Twyford Banbury OX17 3JU
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01364/F
                                    <span class="divider">|</span>










                                    Received:
                                    Mon 30 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Mon 30 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCOIO7EM0N400&amp;activeTab=summary">
                                    Improvement of community facilities compromising construction of disabled toilet, garage for community minibus, bi-fold door and patio
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Wendlebury Village Hall Main Street Wendlebury Bicester OX25 2PS
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01363/F
                                    <span class="divider">|</span>










                                    Received:
                                    Mon 30 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Wed 15 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCOT5BEMFMK00&amp;activeTab=summary">
                                    Single story rear extension; two storey side extension; front porch
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    23 Field Close Kidlington OX5 2HH
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01365/F
                                    <span class="divider">|</span>










                                    Received:
                                    Mon 30 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Mon 30 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCOT64EMFMN00&amp;activeTab=summary">
                                    Like for like replacement of 2 x hardwood ground floor windows to the front of property (either side of front door). Current windows have severe rot and are not repairable. Currently single glazed, and propose to use heritage secondary glaze as has already been approved and installed in the far right (kitchen) window. Replacement of 1 narrow metal and hardwood ground floor window to the front of property (far left). The wood has been subject to rot and damage, and the metal fan light is also damaged and not in-keeping with the property. Propose replacement of a more in-keeping and fully opening window with 2 horizontal bars, as opposed to 3 bar with fan light. The design will match the other two original windows on the ground floor and the window already replaced on the far right. Propose to use heritage secondary glazing as above.
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Thorn Cottage 13 Bainton Road Bucknell Bicester OX27 7LT
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01367/LB
                                    <span class="divider">|</span>










                                    Received:
                                    Mon 30 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Wed 15 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCOT77EMFMO00&amp;activeTab=summary">
                                    Discharge of Conditions 3 (roof slate samples) 5 (external window and doors) and 8 (bird boxes) of 18/00352/F
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    10 High Street Banbury OX16 5DZ
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00304/DISC
                                    <span class="divider">|</span>










                                    Received:
                                    Mon 30 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Mon 30 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCOT7BEMFMP00&amp;activeTab=summary">
                                    Replace the 5 windows at the front of the house
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Middle Cottage St Marys Walk North Aston Bicester OX25 6AA
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01368/LB
                                    <span class="divider">|</span>










                                    Received:
                                    Mon 30 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Mon 30 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCOT82EMFMX00&amp;activeTab=summary">
                                    Variation of condition 2 (plans) of 18/00366/F
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Akeman Spinney Heyford Road Kirtlington Kidlington OX5 3HS
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01374/F
                                    <span class="divider">|</span>










                                    Received:
                                    Mon 30 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Tue 31 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCIZYEEM0N100&amp;activeTab=summary">
                                    New fishing lake
                                </a>









                                <p class="address">


                                    Nell Bridge Farm Aynho Road Kings Sutton OX17 3NY Outside Of Area Outside Of Area Oxfordshire
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01358/ADJ
                                    <span class="divider">|</span>










                                    Received:
                                    Fri 27 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Fri 27 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Decided







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCIKJQEM0K200&amp;activeTab=summary">
                                    Erection of a proposed 5 bedroom detached house
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Plot 288 Phase 1 1B Graven Hill Circular Road Ambrosden
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00048/CC
                                    <span class="divider">|</span>










                                    Received:
                                    Fri 27 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Fri 27 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Decided







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCIZ6VEM0N100&amp;activeTab=summary">
                                    Side extension - re-submission of 18/00400/F
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    81 Walton Avenue Twyford Banbury OX17 3LA
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01357/F
                                    <span class="divider">|</span>










                                    Received:
                                    Fri 27 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Fri 27 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCJ964EMFMD00&amp;activeTab=summary">
                                    Change of Use of the first floor accommodation from D1 to B1
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Bicester Clinic 5 Causeway Bicester OX26 6AN
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01361/F
                                    <span class="divider">|</span>










                                    Received:
                                    Fri 27 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Fri 27 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCJ96TEMFME00&amp;activeTab=summary">
                                    Discharge of Conditions 5 (schedule of external surfaces), 9 (amended plans for certain plots), 11 (amended landscaping scheme), 13 (ground levels), 15 (vehicular accesses, driveways and turning areas), 16 (vehicle tracking), 17 (surface water drainage scheme) and 19 (pedestrian access) of 17/00708/F
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Orginal Plot Numbers 73 To 86 Hanwell FIelds Warwick Road Banbury
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00302/DISC
                                    <span class="divider">|</span>










                                    Received:
                                    Fri 27 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Fri 27 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCJ97FEMFMF00&amp;activeTab=summary">
                                    Discharge of Conditions 3 (arboricultural method statement), 4 (materials), 5 (boundaries) and 6 (access) of 16/01540/F
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Oxford Cottage Oxford Road Wendlebury Bicester OX25 2PT
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00303/DISC
                                    <span class="divider">|</span>










                                    Received:
                                    Fri 27 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Fri 27 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCJ97OEMFMG00&amp;activeTab=summary">
                                    Working from the ground and by way of chain and pole saw cut back various branches which are leaning out over the access road to site and starting to cause concerns for the tankers entering site with all arisings being chipped and spread back neatly under existing trees located of site.
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Water Works South Of Manor Farm Station Road Lower Heyford
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01362/TCA
                                    <span class="divider">|</span>










                                    Received:
                                    Fri 27 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Fri 27 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCJ64HEM0N100&amp;activeTab=summary">
                                    Single storey side extension
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    13 Austin Road Bodicote Banbury OX15 4AS
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01360/F
                                    <span class="divider">|</span>










                                    Received:
                                    Fri 27 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Fri 27 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCJ56IEM0N100&amp;activeTab=summary">
                                    3 bed detached dwelling
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Plot 312 Phase 1 1B Graven Hill Circular Road Ambrosden
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00049/CC
                                    <span class="divider">|</span>










                                    Received:
                                    Thu 26 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Thu 26 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCJ4A8EM0N100&amp;activeTab=summary">
                                    Discharge of condition 3 (external walls finishes and materials) of 18/000742/F
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    17 Cherrys Close Bloxham Banbury OX15 4TD
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00301/DISC
                                    <span class="divider">|</span>










                                    Received:
                                    Thu 26 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Thu 26 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCIRRZEM0N100&amp;activeTab=summary">
                                    Non-material amendment to 17/00561/F - Change of glazing for the dining room doors and kitchen window
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Middle Cottage St Marys Walk North Aston Bicester OX25 6HX
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00094/NMA
                                    <span class="divider">|</span>










                                    Received:
                                    Thu 26 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Thu 26 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCIRIEEM0N100&amp;activeTab=summary">
                                    Non-Material Amendment - Reduce the footprint of the pool house plan and amend the roof design to be a single roof over the revised footprint. In addition, whilst this is not subject to any existing condition to the approval, it is proposed that the heat source for the new house is derived from a ground source heat pump. Energy will be extracted from the ground using sealed boreholes. At least one borehole will be used for water extraction as well (Proposed as non-material amendment to 13/01186/F)
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Paradise Farm Wigginton Road South Newington Banbury OX15 4JS
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00093/NMA
                                    <span class="divider">|</span>










                                    Received:
                                    Thu 26 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Mon 30 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCIOZLEM0N100&amp;activeTab=summary">
                                    Demolition of extensions and erection of two storey extension to rear with no net increase in floor area
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Colworth Cottage 3 Oxford Road Hampton Poyle Kidlington OX5 2QD
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01356/F
                                    <span class="divider">|</span>










                                    Received:
                                    Thu 26 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Thu 26 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCHEHJEMFLZ00&amp;activeTab=summary">
                                    Reserved Matters to 16/01802/OUT - Detailed plot design (scale, appearance, layout, and landscaping) for plots 185-190
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Wave 3 Plot 185-190 Graven Hill Circular Road Ambrosden
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01348/REM
                                    <span class="divider">|</span>










                                    Received:
                                    Thu 26 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Mon 30 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCHEKWEMFM900&amp;activeTab=summary">
                                    Renault and DACIA Signs, 3 x Fascias
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Jay Bee Motors Oxford Road Bodicote Banbury OX15 4AB
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01355/ADV
                                    <span class="divider">|</span>










                                    Received:
                                    Thu 26 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Thu 02 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCHEIQEMFM100&amp;activeTab=summary">
                                    Variation of condition 1 (documents and drawings) of 16/00604/F - Landscape design - The developed landscape design incorporates the principle of the first design which is that the more geometric gardens immediately around the house become softer as they merge with the wider landscape. This design adds more detail to areas such as the north bank, the yoga garden outside the gym, the vegetable garden and the sports areas to the South East. Energy Centre - the energy centre has been relocated to a more appropriate location to the South East
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Paradise Farm Wigginton Road South Newington Banbury OX15 4JS
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01350/F
                                    <span class="divider">|</span>










                                    Received:
                                    Thu 26 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Mon 30 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCHEJ1EMFM200&amp;activeTab=summary">
                                    Two new dormer windows to replace two existing rooflights
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    The Parlour Church Farm Chapel Lane Milton Banbury OX15 4HH
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01351/F
                                    <span class="divider">|</span>










                                    Received:
                                    Thu 26 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Fri 27 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCHEJBEMFM300&amp;activeTab=summary">
                                    Alteration to the existing access gates into Beech House to enlarge the width of the opening and replace the original passing gate with a new timber gate. The proposed works are to be undertaken to a wall which is considered to be within the curtilage of an adjacent Grade II listed property, The Hermitage, Souldern
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Beech House High Street Souldern Bicester OX27 7JL
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01352/LB
                                    <span class="divider">|</span>










                                    Received:
                                    Thu 26 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Thu 26 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCHEJOEMFM400&amp;activeTab=summary">
                                    T1 - Maple - Pollard to 7 ft. T2 - Maple - Crown lift to 7ft.
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Calder Cottage Philcote Street Deddington Banbury OX15 0TB
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00202/TCA
                                    <span class="divider">|</span>










                                    Received:
                                    Thu 26 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Thu 26 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCHEKCEMFM500&amp;activeTab=summary">
                                    Knock off existing cementitious render to 2 no. external walls (1 no. wall to side of property and 1 no. wall to rear of property). Re-render using a breathable lime render
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    The Old George And Dragon High Street Charlton On Otmoor Kidlington OX5 2UG
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01353/F
                                    <span class="divider">|</span>










                                    Received:
                                    Thu 26 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Tue 14 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCHEKGEMFM600&amp;activeTab=summary">
                                    Knock off existing cementitious render to 2 no. external walls (1 no. wall to side of property and 1 no. wall to rear of property). Re-render using a breathable lime render
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    The Old George And Dragon High Street Charlton On Otmoor Kidlington OX5 2UG
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01354/LB
                                    <span class="divider">|</span>










                                    Received:
                                    Thu 26 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Tue 14 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCHEIEEMFM000&amp;activeTab=summary">
                                    Erection of an office and welfare accommodation building
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Thames Water Regional Design Centre Langford Lane Kidlington OX5 1HT
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01349/F
                                    <span class="divider">|</span>










                                    Received:
                                    Thu 26 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Thu 02 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCFJUSEMFLK00&amp;activeTab=summary">
                                    Discharge of Condition 25 (public art) of 15/01012/OUT
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Link 9 Land North East Of Skimmingdish Lane Launton
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00297/DISC
                                    <span class="divider">|</span>










                                    Received:
                                    Wed 25 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Wed 25 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCFJW8EMFLL00&amp;activeTab=summary">
                                    Discharge of Conditions 11 (archaeology), 12 (drainage scheme), 15 (CEMP) and 16 (CEMP Ecology) of 14/02156/OUT
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Land South Of Cotefield Business Park Phase 2 Adj To Blossom Field Road Bodicote
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00298/DISC
                                    <span class="divider">|</span>










                                    Received:
                                    Wed 25 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Wed 25 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCFJWVEMFLM00&amp;activeTab=summary">
                                    Variation of condition 7 (no new windows) of 08/00245/F - vary the condition to allow for the increased size and different position of the roof-lights
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Barn 1 Shutford Grounds Farm Epwell Road Shutford
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01342/F
                                    <span class="divider">|</span>










                                    Received:
                                    Wed 25 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Wed 25 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCFJXNEMFLO00&amp;activeTab=summary">
                                    Discharge of Conditions 13 (Lighting scheme) and 28 (Connections to Salt Way) of 17/00669/REM
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    OS Parcels 1200 3100 2000 1981 South Of Salt Way Bloxham Road Banbury
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00299/DISC
                                    <span class="divider">|</span>










                                    Received:
                                    Wed 25 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Wed 25 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCFJYDEMFLP00&amp;activeTab=summary">
                                    Variation of Condition 2 (drawings) of 16/01214/F- The application seeks to vary condition 2 by replacing reference to the plans with revised drawings 15099-P01-C; 15099-P02-E and 15099-P03-D.
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Stonecroft 15 Oxford Road Hampton Poyle Kidlington OX5 2QD
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01344/F
                                    <span class="divider">|</span>










                                    Received:
                                    Wed 25 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Mon 13 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCF8Y4EM0N100&amp;activeTab=summary">
                                    Certificate of Lawfulness of Proposed Development for the erection of a two storey rear extension
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    13 Old Glebe Upper Tadmarton Banbury OX15 5TH
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01341/CLUP
                                    <span class="divider">|</span>










                                    Received:
                                    Wed 25 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Wed 25 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCFJYVEMFLR00&amp;activeTab=summary">
                                    T1 x Cherry plum - Overall Crown reduction by up to 4.0m.
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Rose Villa 17 Cavendish Place Stratton Audley Bicester OX27 9BN
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00200/TCA
                                    <span class="divider">|</span>










                                    Received:
                                    Wed 25 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Wed 25 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCFJZ4EMFLS00&amp;activeTab=summary">
                                    Erection of two dwellings
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Land South West Of South Riding Station Road Launton
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01345/F
                                    <span class="divider">|</span>










                                    Received:
                                    Wed 25 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Wed 25 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCFJZFEMFLT00&amp;activeTab=summary">
                                    Certificate of Lawfulness of Proposed Development for erection of a single rear storey extension
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    39 Wensum Crescent Bicester OX26 2GL
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01346/CLUP
                                    <span class="divider">|</span>










                                    Received:
                                    Wed 25 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Mon 30 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCFJZNEMFLU00&amp;activeTab=summary">
                                    T1 x Lime - Cut back from the house to leave up to a 3.0 metre clearance and lift over the road to 5.0 metres. Remove major deadwood (exempt). T2 x Yew -Cut back from the house to leave a 2.0 metre clearance.
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Hamelin Cottage Horn Hill Road Adderbury Banbury OX17 3EU
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00201/TCA
                                    <span class="divider">|</span>










                                    Received:
                                    Wed 25 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Wed 25 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCF4EXEM0N100&amp;activeTab=summary">
                                    Erection of a general purpose agricultural shed
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    OS Parcel 7648 NE Of Blackthorn Hill Adjoining Blackthorn Road Launton
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01339/AGN
                                    <span class="divider">|</span>










                                    Received:
                                    Wed 25 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Wed 25 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCFJYNEMFLQ00&amp;activeTab=summary">
                                    Discharge of Condition 3 (Travel Plan) of 18/00556/F
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Glenmore Business Park Langford Locks Kidlington
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00300/DISC
                                    <span class="divider">|</span>










                                    Received:
                                    Wed 25 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Wed 25 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCDPBNEMFLC00&amp;activeTab=summary">
                                    Two storey rear extension
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Langway Villa Bell Street Hornton Banbury OX15 6DB
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01335/F
                                    <span class="divider">|</span>










                                    Received:
                                    Tue 24 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Mon 13 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCDPBVEMFLD00&amp;activeTab=summary">
                                    Certificate of Lawfulness of Proposed Development for removal of conservatory and erection of single-storey rear extension
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    6 Orchard Piece Mollington Banbury OX17 1DP
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01336/CLUP
                                    <span class="divider">|</span>










                                    Received:
                                    Tue 24 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Mon 06 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCDPC4EMFLE00&amp;activeTab=summary">
                                    Discharge of Condition 3 (parking) of 18/00594/F
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Chequer Tree Farm Ells Lane Bloxham Banbury OX15 5EE
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/00296/DISC
                                    <span class="divider">|</span>










                                    Received:
                                    Tue 24 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Tue 24 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCDBYUEM0N100&amp;activeTab=summary">
                                    Erection of essential workers dwelling - resubmission of 18/00279/F
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Stud Farm Kirtlington Road Chesterton Bicester OX26 1TF
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01331/F
                                    <span class="divider">|</span>










                                    Received:
                                    Tue 24 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Tue 24 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCDPCUEMFLG00&amp;activeTab=summary">
                                    Erection of a 1.75m diameter micro wind turbine on the north gable of the main barn to help provide power to the off-grid property
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Hadsham Barn Hornton Lane Horley Banbury OX15 6BN
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01338/F
                                    <span class="divider">|</span>










                                    Received:
                                    Tue 24 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Mon 13 Aug 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>



                            <li class="searchresult">

                                <a href="/online-applications/applicationDetails.do?keyVal=PCDPAQEMFLA00&amp;activeTab=summary">
                                    Extension to existing Technical Site to provide new employment units comprising flexible B1(c) light industrial, B2 (general industrial), B8 (storage or distribution) uses with ancillary offices, storage, display and sales, together with associated access, parking and landscaping
                                </a>


                                <span class="canCommentIndicator">
                    <img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/ico_comment.gif" alt="Open for comment icon"/>
                </span>








                                <p class="address">


                                    Bicester Heritage Buckingham Road Bicester
                                </p>


                                <p class="metaInfo">
                                    Ref. No:
                                    18/01333/F
                                    <span class="divider">|</span>










                                    Received:
                                    Tue 24 Jul 2018
                                    <span class="divider">|</span>












                                    Validated:
                                    Tue 24 Jul 2018
                                    <span class="divider">|</span>






                                    Status:
                                    Awaiting decision







                                </p>





                            </li>


                        </ul>


                    </div>







                    <div class="clear"></div>

                    <p class="pager bottom"><span class="showing"><strong>Showing 1-100</strong> of 817</span><span class="divider">|</span><strong>1</strong><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=2" class="page">2</a><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=3" class="page">3</a><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=4" class="page">4</a><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=5" class="page">5</a><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=6" class="page">6</a><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=7" class="page">7</a><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=8" class="page">8</a><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=9" class="page">9</a><span class="divider">|</span><a href ="/online-applications/pagedSearchResults.do?action=page&amp;searchCriteria.page=2" class="next">Next<img border="0" src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/pager_next.gif" alt="" title="Display the next page of search results"/></a></p>

                    <div class="clear"></div>
                </div>

                <!-- #EndEditable -->
                <!-- #BeginEditable "pagefoot" --><!-- #EndEditable -->
            </div>
            <!-- END WAM CONTENT -->

            <p id="poweredBy"><a href="http://www.idoxgroup.com/" target="_blank" title="This link will open a new window"><img src="https://www.publicaccess.cherwell.gov.uk/online-applications-skin/images/idox.gif" alt="an Idox solution" /></a></p>
        </div>
        <div id="footerTop"></div>
        <div id="footer">
            <div class="container">
                <div id="links">
                    <ul>
                        <li><a href="https://www.cherwell.gov.uk/accessibility">Accessibility</a></li><li><a href="https://www.cherwell.gov.uk/info/5/your-council/367/privacy-and-cookies">Privacy and cookies</a></li><li><a href="https://www.cherwell.gov.uk/site-map">Site map</a></li><li><a href="https://www.cherwell.gov.uk/contact">Contact us</a></li><li><a href="https://www.cherwell.gov.uk/info/5/your-council/368/terms-and-disclaimer">Terms and disclaimer</a></li>
                    </ul>
                </div>
                <div id="footercontent"><p><script type="text/javascript">document.write(new Date().getFullYear());</script> &copy; Cherwell District Council Bodicote House, Bodicote, Banbury OX15 4AA</p></div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</div>
<!-- IDOX PA END -->

</body>
</html>

